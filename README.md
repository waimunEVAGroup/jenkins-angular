# jenkins-angular

This is a Tutorial to configure Jenkins to automate building of AngularJS and Angular project using Pipeline.

In a nutshell, use Jenkins to automate your development workflow so you can focus on work that matters most. Jenkins is commonly used for:

- Building projects
- Running tests to detect bugs and other issues as soon as they are introduced
- Static code analysis
- Deployment

Execute repetitive tasks, save time, and optimize your development process with Jenkins.

# Downloads
Non-source downloads such as WAR files and several Linux packages can be found on our [Mirrors].

```
A copy is in the ./bin folder.
```

# Prerequisites
You will require:

- A machine with:
256 MB of RAM, although more than 512MB is recommended
- 10 GB of drive space (for Jenkins)
- Java 8 (either a JRE or Java Development Kit (JDK) is fine)

# Installation
You will require:

1. Run `java -jar jenkins.war --httpPort=8080`
2. Browse to http://localhost:8080.
3. Follow the instructions to complete the installation.

# Plugins
You are also require to install the [Push Over SSH] plugin.

# AngularJS
After creating a `Freestyle project`, you add the following `Add build step`. 

Depending on the environment, select the `Execute Windows batch command`, or `Execute shell` accordingly.

```bash
npm install
```
```bash
npm run test-single-run
```
```bash
npm run deploy
```
```bash
scp -r public/* target
```

# Angular.io
After creating a `Freestyle project`, you add the following `Add build step`. 

Depending on the environment, select the `Execute Windows batch command`, or `Execute shell` accordingly.

```bash
ng test
```
```bash
ng build
```
```bash
scp -r dist/* target
```

# Note

The `scp` command copy the distribution codes to the public folder of the web server. 

For remote server, use the `Add post-build action`, `Send build artifacts over SSH` instead.

[Mirrors]: http://mirrors.jenkins-ci.org
[GitHub]: https://github.com/jenkinsci/jenkins
[website]: https://jenkins.io/
[@jenkinsci]: https://twitter.com/jenkinsci
[wiki]: https://wiki.jenkins-ci.org
[Push Over SSH]: http://wiki.jenkins-ci.org/display/JENKINS/Publish+Over+SSH+Plugin
